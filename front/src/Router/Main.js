/*eslint-disable import/first*/
import React,{ Component } from "react";
import thunk from 'redux-thunk'
import {Router, Route, Switch,Redirect} from 'react-router-dom'
import createHistory from 'history/createBrowserHistory'
import HomePage from '../components/homepage/HomePage.js';
//import AdminPage from'../components/adminpage/AdminPage'
import NotFoundPage from '../components/notfoundpage/NotFound'
import ReadPage from '../components/readpage/ReadPage'
import AuthenticatePage from '../components/AuthentificationPage/AuthentificationPage'
import UserPage from '../components/UsersPage/UserPage'
import WritePage from '../components/WritePage/WritePage.js'
import AdminPageContainer from '../components/adminpage/adminPageContainer'
import AdminControll from '../components/adminpage/Admin/adminControl'
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
// Redux store part 
import rootReducer from '../Store/reducers';
export const store = createStore(rootReducer,composeWithDevTools(applyMiddleware(thunk)));
store.subscribe (()=>console.log('Log from subscribe',store.getState()));

// Evrything for Article, remove to components folder
import PageBasic from '../components/Layout/layout'
import {Link} from 'react-router-dom';
import {Icon} from 'antd'
//import delay from '../delay'
import {Typography, Divider} from "antd";
const { Title } = Typography;
import ReactMarkdown from 'react-markdown';
class Article extends Component{
  constructor(props){
    super(props);
    this.state = {
        title : '',
        description :'',
        article : ''
    }
  }
  async componentDidMount(){
    var url = this.props.match.params.article_title;
    console.log(url);
    let data = await ( await fetch(`http://localhost:5000/readpage/${url}`)).json()
    console.log(data)
    this.setState ({title : data.title})
    this.setState ({description : data.description})  
    this.setState ({article : data.article})
  }

  render(){ 
    //console.log(this.state)
    return (
      <PageBasic>
        <div className='article-page'>
          <div>
            <Title> {this.state.title}</Title>
            <Divider/>
            <Title level={2}> {this.state.description} </Title> 
            <ReactMarkdown source={this.state.article} />
          </div>
          <Link to='/readpage' className='back-arrow'>
            <Icon type="double-left"  style={{fontSize :'30px'}} /> 
          </Link>  
        </div>
      </PageBasic>
    )
  }
}

class Main extends Component{
    render(){
        return(
          <Provider store={store}>
            <Router history={createHistory()}>
              <Switch> 
              <Redirect from="/main" to="/"/>
                <Redirect from="/readpage/writepage" to="/writepage"/>
                <Route path="/" component={HomePage} exact/>
                <Route path='/readpage' component={ReadPage} exact/>
                <Route path='/readpage/:article_title' component={Article} exact />
                <Route path='/writepage' component={WritePage} exact/>
                <Route path='/usersinfo'component={UserPage} exact/>
                <Route path='/authenticate' component={AuthenticatePage} exact/>
                <Route path='/admin' component={AdminPageContainer} exact />
                <Route path='/admin/controll' component={AdminControll} exact />
                <Route component={NotFoundPage} exact/>
              </Switch>
            </Router>
          </Provider>
        )
    }
}
export default Main;