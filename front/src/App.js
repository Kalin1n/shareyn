/*eslint-disable import/first*/
import React from 'react'; 
import './App.css';  
//eslint-disable import/first
import 'antd/dist/antd.css'; 

import Main from './Router/Main';

const App = () => (
      <div className="App"> 
          <Main/>
      </div>
)

export default App;
