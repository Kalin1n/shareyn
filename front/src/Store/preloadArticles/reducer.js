
import {GET_ARTICLES} from './actions'

const defaultState = {
    articles : []
}

export const prewievArticlesReducer = ( state = defaultState, action ) => {
    switch(action.type){
        case GET_ARTICLES : 
            return {...state, articles : action.payload}
        default : return state;
    }
}