import { combineReducers} from 'redux';
import { registerReducer } from './register/reducers';
import { signInReducer } from './signIn/reducers';
import { articleReducer } from './article/reducers';
import { adminReducer } from './admin/reducer'
export default combineReducers({
    signIn : signInReducer,
    register : registerReducer,
    article : articleReducer,
    admin : adminReducer
});
    