/*eslint-disable import/first*/
import React,{ Component } from "react";
import {Divider,Typography, Tabs} from 'antd';
const {Title} = Typography;
const TabPane = Tabs.TabPane;
import PageBasic from '../Layout/layout'
import SignInContainer from './SignIn/SignInContainer'
import RegisterContainer from './Register/RegisterContainer'
class AuthentificationPage extends Component{
    render(){
        return(
            <PageBasic>
                <Title underline='true'> Sign In / Register...</Title>
                <Tabs>
                  <TabPane tab='Sign In' key='1'>
                    <div className="form-auth">
                        <h1> Sign In </h1>
                        <SignInContainer />
                    </div>
                  </TabPane>
                  <TabPane tab='Register' key='2'>
                     <Divider type="vertical" dashed='true'/>
                    <div className="form-auth">
                        <h1> Register </h1>
                        <RegisterContainer />
                    </div>
                  </TabPane>
                </Tabs>
            </PageBasic>
        )
    }
}
export default AuthentificationPage;