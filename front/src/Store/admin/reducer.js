import {ADMIN_LOGIN_CHANGE, ADMIN_PASSWORD_CHANGE} from './actions';

const defaultState = {
    login : '',
    password : '',
    payload : null,
    error : null,
    status : null
};

export const adminReducer = ( state = defaultState, action )=>{
    switch (action.type){
        case ADMIN_LOGIN_CHANGE : 
            return { ...state, login : action.payload }
        case ADMIN_PASSWORD_CHANGE : 
            return { ...state, password : action.payload }
        default :
            return state
    }
}