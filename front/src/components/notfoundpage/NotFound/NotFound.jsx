/*eslint-disable import/first*/
import React, {Component}from 'react';
import { Divider, Typography} from 'antd';
const {Title} = Typography;
import {Link} from 'react-router-dom';

class NotFound extends Component{  // Work, need CSS
  render(){
    return(
      <div className="bad-url-page">
        <Title > (╯°□°)╯︵ your URL is @#!*&%?$</Title>
        <Title underline='true' mark='true'>Bad URL, try another adress </Title>
        <Divider />
        <Title type='danger'> 4Ø4 ERRØR</Title>
        <Title level={3}> Or visit our home page by this link  <Link to='/'> (　＾∇＾)</Link>  </Title>
      </div>
    )
  }
}

export default NotFound;
