export const ARTICLE_CHANGE_TITLE = 'ARTICLE_CHANGE_TITLE';
export const ARTICLE_CHANGE_DESCRIPTION = 'ARTICLE_CHANGE_DESCRIPTION';
export const ARTICLE_CHANGE_VALUE = 'ARTICLE_CHANGE_VALUE';
export const SEND_ARTICLE = 'SEND_ARTICLE';
export const SET_ARTICLE_STATUS = 'SET_ARTICLE_STATUS'

export const setArticleTitle = ( title ) => ({
    type : ARTICLE_CHANGE_TITLE,
    payload : title 
})

export const setArticleDescription = ( description )=> ({
    type : ARTICLE_CHANGE_DESCRIPTION,
    payload : description
})

export const setArticleValue = ( value ) => ({
    type :  ARTICLE_CHANGE_VALUE,
    payload : value
})

export const articlePending = () => ({
    type : SET_ARTICLE_STATUS,
    status : 'ARTICLE PENDING',
    articlePayload : null,
    error : null
})

export const articleResolved = ( payload ) => ({
    type : SET_ARTICLE_STATUS,
    status : 'ARTICLE RESOLVED',
    created : true,
    articlePayload : payload,
    error : null
})

export const articleRejected = ( error ) => ({
    type : SET_ARTICLE_STATUS,
    status : 'ARTICLE REJECTED',
    created : false,    
    error
})

export function createArticle (title, description, value) {
    return async dispatch => {
        dispatch(articlePending())
        console.log('Create article otrabativaet', title, description, value);
        var data = await ( await fetch('http://localhost:5000/createArticle', {
            headers : {
                'Content-Type':"application/json",
                'Accept' :'application/json'
            },
            method : 'POST',
            body : JSON.stringify({title, description, value})
        })).json()
        console.log('Data priexala', data)
        if (data.created){
            dispatch(articleResolved(data.created))
        }
        else{
            dispatch(articleRejected(data.error))
        }
        return {
            type : SET_ARTICLE_STATUS,
            payload : data
        }
    }
}