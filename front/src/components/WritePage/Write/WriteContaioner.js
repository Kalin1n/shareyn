import React, {Component} from 'react';
import TextEditor from './Write';
import { connect } from 'react-redux';
import {setArticleDescription, setArticleTitle, setArticleValue, createArticle} from '../../../Store/article/actions'
class WriteContainer extends Component {
    render(){
        return (
            <TextEditor 
                title = { this.props.title }
                description = { this.props.description }
                value = { this.props.value }
                setArticleValue = { this.props.setArticleValue }
                setArticleTitle = { this.props.setArticleTitle }
                setArticleDescription = { this.props.setArticleDescription }
                createArticle = { this.props.createArticle }
                status = { this.props.status }
                error = {this.props.error}
            />
        )
    }
}
const mapStateToProps = (state) => {
    return {
        title : state.article.title,
        description : state.article.description,
        value : state.article.value,
        status : state.article.status
    };
}
const mapDispatchToProps = {
    setArticleDescription, setArticleTitle, setArticleValue, createArticle
};

export default connect(mapStateToProps, mapDispatchToProps)(WriteContainer);