import React, {Component} from 'react';
import {Form, Input,Icon, Button, Alert, Typography, Divider} from 'antd';
const {Title} = Typography;
class Register extends Component{
  constructor(props){
    super(props);
    this.realNameChange = this.realNameChange.bind(this);
    this.surnameChange = this.surnameChange.bind(this);
    this.emailChnage = this.emailChnage.bind(this);
    this.passwordChange = this.passwordChange.bind(this);
    this.nicknameChange = this.nicknameChange.bind(this);
    this.passwordChange = this.passwordChange.bind(this);
    this.checkPasswordChange = this.checkPasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  realNameChange(event){
    this.props.setNameText(event.target.value)
  }
  surnameChange(event){
    this.props.setSurnameText(event.target.value)
  }
  emailChnage(event){
    this.props.setEmailText(event.target.value)
  }
  nicknameChange(event){
    this.props.setNicknameText(event.target.value)
  }
  passwordChange(event){
    this.props.setPasswordText(event.target.value)
  }
  checkPasswordChange(event){
    this.props.setPasswordChangeText(event.target.value)
  }
  handleSubmit(event){
    this.props.register(this.props.name, this.props.surname, this.props.email, this.props.nickname, this.props.password)
  
   console.log(this.state)
  }
  render(){ 
    return(
      <div>
        <div> 
          {
            this.props.status === 'RESOLVED' ? 
            <div>
            <Alert type='success' message= {
                <Title> 
                   <Icon type="check-circle" style={{ fontSize: '50px'}} />
                   Sign In as {this.props.nickname}
                </Title>
            }> 
              </Alert>
              <Divider/>  
          </div> : <div>  
              <Title type='danger' level={3}>
               
                {this.props.error === null ? <div> </div> :  <div> 
                  <Alert type='error' message={
                    <div>
                    
                    <Title type='danger' level={3}> {this.props.error} </Title>
                    </div>
                    }  />
                  
                  </div> }
              </Title>
          </div>
          }
          </div>
        <Form>
          <Form.Item label="Your name and surname and e-mail">
            <Input 
              prefix={
                <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }}/>
                } 
              value={this.props.name} 
              onChange={this.realNameChange}
              placeholder="Your name" />
            <Input 
              prefix={
                <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }}/>
                  } 
              value={this.props.surname} 
              onChange={this.surnameChange}
              placeholder="Your surname" />
            <Input 
              prefix={
                <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }}/>
                  } 
              value={this.props.email} 
              onChange={this.emailChnage}
              placeholder="Your e-mail"  />
          </Form.Item>
          <Form.Item label="Think about your nickaname">
            <Input 
              prefix={
                <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }}/>
              } 
              value={this.props.nickname} 
              onChange={this.nicknameChange}
              placeholder="Nickname" />
            <Input 
              prefix={
                <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }}/>
                } 
              value={this.props.password} 
              onChange={this.passwordChange} 
              placeholder="Password"  
              type="password"/>
            <Input 
              prefix={
                <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }}/>
                } 
              value={this.props.passwordCheck} 
              onChange={this.checkPasswordChange} 
              placeholder="Input password again"  
              type="password"/>
          </Form.Item>  
          <Button type="primary" htmlType="submit" onClick={this.handleSubmit}> Register </Button>
        </Form>
      </div>
    )
  }
};
export default Register;