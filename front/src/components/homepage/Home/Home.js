import React, {Component} from 'react';
import {Typography, Divider,Icon} from 'antd';
const {Title, Text} = Typography;

class Home extends Component {
  render (){
    return (
      <div className="home-page">
        <Title mark='true'> # ShareYN </Title >
        <Divider />
        <Title level={2} >
          Create articles and blogs using
          <Text code >
            Markdown<Icon type="arrow-down" />
          </Text>
          markup language
        </Title>
        <Divider />
        <Title level={3}>About   <Text code>
          Markdown<Icon type="arrow-down" />
          </Text> :
          <a className="hplinks"href="https://ru.wikipedia.org/wiki/Markdown" target="_blank" rel="noopener noreferrer"> visit this link </a>
        </Title>
        <Divider />
        <Title level={3}> Basic   <Text code>
          Markdown<Icon type="arrow-down" />
          </Text> syntax rules :
          <a className="hplinks"href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet" target="_blank"rel="noopener noreferrer"> visit this link </a>
        </Title>
    </div>
  )
  }
}
export default Home;
