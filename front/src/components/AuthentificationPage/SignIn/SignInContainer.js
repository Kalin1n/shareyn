import React, {Component} from 'react';
import SignIn from './SignIn';
import { connect } from 'react-redux';
import {setNicknameText, setPasswordText, signIn} from '../../../Store/signIn/actions'
class SignInContainer extends Component {
    render(){
        return(
            <SignIn 
                nickname={this.props.nickname} 
                password={this.props.password} 
                setNicknameText={this.props.setNicknameText} 
                setPasswordText={this.props.setPasswordText}
                error ={this.props.error}
                signIn={this.props.signIn}
            />
        )
    }
}

const mapStateToProps = (state) => {
    //console.log('ms2p signin',state)
    return{
        nickname : state.signIn.nickname,
        password : state.signIn.password,
        error : state.signIn.error
    };
};
const mapDispatchToProps = {
        setNicknameText, setPasswordText, signIn
};

export default connect(mapStateToProps, mapDispatchToProps)(SignInContainer);
