/*eslint-disable import/first*/
import React from 'react';
import {Layout,  Menu, Icon} from 'antd';
const { Header, Content, Footer } = Layout
import {Link} from 'react-router-dom'

import Logo from './Logo/Logo'

let PageBasic = props =>
    <Layout className="layout">
      <Header className="header" >
        <Logo/>
        <Menu
          theme="dark"
          mode="horizontal"
          style={{ lineHeight: '64px' }}
        >
          <Menu.Item key="1"> <Link to='/'> <Icon type="home" />Home</Link> </Menu.Item>
          <Menu.Item key="2"> <Link to='/readpage'><Icon type="book" />Read</Link> </Menu.Item>
          <Menu.Item key="3"> <Link to='writepage'> <Icon type="file-markdown" />Write</Link> </Menu.Item>
          <Menu.Item key="4"> <Link to='/usersinfo'><Icon type="sliders" />Users desk</Link> </Menu.Item>
          <Menu.Item key="5"> <Link to='/authenticate'><Icon type="idcard" />Register / Sign In</Link> </Menu.Item>
        </Menu>
      </Header>
      <Content style={{ padding: '0 50px' }} className="main">
        <div style={{ background: '#fff', padding: 24, minHeight: 800 }} className="content">
          {props.children}
        </div>
      </Content>
      <Footer style={{ textAlign: 'center' }} className="footer">
        ShareYN Created by Kalinin Ivan 2019 <br/>
        Ant Design ©2018 Created by Ant UED
      </Footer>
    </Layout>

export default PageBasic;
