import React, {Component} from 'react';
import {Form,Icon, Input, Button, Typography, Alert, Divider}from'antd';
const { Title } = Typography;
class SignIn extends Component {
  constructor(){
    super()
    //this.state  = {}
    this.nicknameChange = this.nicknameChange.bind(this);
    this.passwordChange = this.passwordChange.bind(this);
    this.signIn = this.signIn.bind(this)
  }
  nicknameChange(event){
    this.props.setNicknameText(event.target.value);
  }
  passwordChange (event){
    this.props.setPasswordText(event.target.value);
  }
  signIn (){
    var nickname = this.props.nickname;
    var password = this.props.password;
    console.log('AABujub',nickname, password)
    this.props.signIn(nickname,password) 
  }
  render() {
    return (
      <div>
       <div >
        { 
          this.props.error ===null ? 
            <div></div> 
          : <div> 
              <Alert type='error' 
                message={
                  <Title type='danger' level={3}>
                    <Icon type="warning"style={{ fontSize: '50px'}}/>
                    {this.props.error}
                  </Title>}>
              </Alert>
              <Divider/>
            </div>
        } 
      </div> 
      <Form   className="login-form">
        <Form.Item>
          <Input 
            prefix={
              <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />
            } 
            value={this.props.nickname} 
            onChange={ this.nicknameChange} 
            placeholder="Username" 
          />
        </Form.Item>
        <Form.Item>
          <Input 
            prefix={
              <Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />
            } 
            type="password" 
            placeholder="Password" 
            value = {this.props.password} 
            onChange = { this.passwordChange} 
          />
        </Form.Item>
        <Form.Item>
          <Button onClick={this.signIn}>
            Sign in
          </Button>
        </Form.Item>
      </Form>
     </div>
    );
  }
}


export default SignIn;
