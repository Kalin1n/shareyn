import { 
    ARTICLE_CHANGE_DESCRIPTION,
    ARTICLE_CHANGE_TITLE,
    ARTICLE_CHANGE_VALUE,
    SET_ARTICLE_STATUS
 } from './actions'


 const defaultState = {
     title : '',
     description : '',
     value : '',
     articlePayload : null,
     status : null,
     error : null
 };

 export const articleReducer = ( state = defaultState, action ) => {
     switch (action.type){
        case ARTICLE_CHANGE_TITLE : 
            return {...state, title : action.payload}
        case ARTICLE_CHANGE_DESCRIPTION : 
            return {...state, description : action.payload}
        case ARTICLE_CHANGE_VALUE : 
            return {...state, value : action.payload}
        case SET_ARTICLE_STATUS : 
            return {...state, articlePayload : action.payload, error :action.error, status : action.status}
        default : return state;
     }
 }