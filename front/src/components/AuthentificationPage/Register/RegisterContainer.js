import React, {Component} from 'react';
import Register from './Register';
import {connect} from 'react-redux';
import {setNameText, setSurnameText, setEmailText, 
        setNicknameText, setPasswordText, setPasswordChangeText, register} from '../../../Store/register/actions'

class RegisterContainer extends Component{
    render (){
        return(
            <Register 
                name={this.props.name}
                setNameText={this.props.setNameText}
                surname={this.props.surname}
                setSurnameText={this.props.setSurnameText}
                email={this.props.email}
                setEmailText={this.props.setEmailText}
                nickname={this.props.nickname}
                setNicknameText={this.props.setNicknameText}
                password={this.props.password}
                setPasswordText={this.props.setPasswordText}
                passwordCheck={this.props.passwordCheck}
                setPasswordChangeText={this.props.setPasswordChangeText}
                register={this.props.register}
                error = {this.props.error}
                status = {this.props.status}
            />
        )
    }
}

const mapStateToProps = (state) => {
    return {
        name  : state.register.name,
        surname : state.register.surname,
        email : state.register.email,
        nickname : state.register.nickname,
        password : state.register.password,
        passwordCheck : state.register.passwordCheck,
        error : state.register.error,
        status : state.register.status
    };
};

const mapDispathcToProps = {
    setNameText, setSurnameText, setEmailText, setNicknameText, setPasswordText, setPasswordChangeText
    , register
}
export default connect (mapStateToProps, mapDispathcToProps)(RegisterContainer);