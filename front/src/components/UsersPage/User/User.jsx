/*eslint-disable import/first*/
import React, {Component} from 'react';
import {Form, Typography, Avatar, Modal, Button, Divider, Input, Icon} from 'antd';
const { Title } = Typography;
import AvatarUserChange from './AvatarUserChange'; // Component



class UsersExamp extends Component {
  constructor(){
    super()
    this.state = {
      nicknameModalVisible : false,
      passwordModalVisible : false,
      avatarModalVisible : false
    }
    this.changeNicknameOld = this.changeNicknameOld.bind(this);
    this.changeNicknameNew = this.changeNicknameNew.bind(this);
    this.changePasswordOld = this.changePasswordOld.bind(this);
    this.changePasswordNew = this.changePasswordNew.bind(this);
    this.changePasswordNewCheck = this.changePasswordNewCheck.bind(this);
  }
  setNicknameModalVisible(nicknameModalVisible){
    this.setState({nicknameModalVisible});
    console.log(this.state)
  }
  setPasswordModalVisible(passwordModalVisible){
    this.setState({passwordModalVisible});
    console.log(this.state)
  }
  setAvatarModalVisible(avatarModalVisible){
    this.setState({avatarModalVisible});
  }
  changeNicknameOld(event){
    this.setState({nicknameOld : event.target.value})
    //console.log(this.state)
  }
  changeNicknameNew(event){
    this.setState({nicknameNew : event.target.value})
    //console.log(this.state)
  }
  changePasswordOld(event){
    this.setState({passwordOld : event.target.value})
    //console.log(this.state)
  }
  changePasswordNew(event){
    this.setState({passwordNew : event.target.value})
    //console.log(this.state)
  }
  changePasswordNewCheck(event){
    this.setState({passwordNewCheck : event.target.value})
  //  console.log(this.state)
  }
  render(){
    return (
        <div className='user-info'>
          <div className='user-avatar'>
            <Avatar shape='square' size={128} icon='idcard'  className='user-icon'/>
            <Button type="primary" onClick={()=> this.setAvatarModalVisible(true)}> Change Avatar </Button>
            <Modal
              title="Change your avatar..."
              centered
              visible={this.state.avatarModalVisible}
              onOk={()=> this.setAvatarModalVisible(false)}
              onCancel={()=> this.setAvatarModalVisible(false)}
              >
              <Title level={4}> Upload a new photo... </Title>
              <AvatarUserChange />
            </Modal>
          </div>
          <div className='user-name'>
            <div>
              <p> Real name </p>
              <p> Real surname </p>
              <p> Nickname </p>
              <Button type="primary" onClick={() => this.setNicknameModalVisible(true)}> Change Your nickaname... </Button>
              <Modal
                  title="Change nickname..."
                  centered
                  visible={this.state.nicknameModalVisible}
                  onOk={() => this.setNicknameModalVisible(false)}
                  onCancel={() => this.setNicknameModalVisible(false)}
                >
                <Form>
                  <Form.Item>
                      <Title level={4}> Your old nickname </Title>
                      <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }}/>} value={this.state.nicknameOld} onChange={this.changeNicknameOld}vlplaceholder="Old nickname" />
                  </Form.Item>
                  <Divider />
                  <Form.Item>
                      <Title level={4}> Your new nickname </Title>
                      <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }}/>} value={this.state.nicknameNew} onChange={this.changeNicknameNew}placeholder="New nickname" />
                  </Form.Item>
                </Form>
              </Modal>
              <Divider />
              <Button type='primary' onClick={()=> this.setPasswordModalVisible(true)}> Change your password... </Button>
              <Modal
                title="Change password..."
                centered
                visible={this.state.passwordModalVisible}
                onOk={()=> this.setPasswordModalVisible(false)}
                onCancel={()=> this.setPasswordModalVisible(false)}
              >
                <Form>
                  <Form.Item>
                        <Title level={4}>  Input your old password </Title>
                        <Input prefix={ <Icon type="warning" style={{ color: 'rgba(0,0,0,.25)' }}/>} value={this.state.passwordOld} onChange={this.changePasswordOld}type="password" placeholder="Your old password"/>
                  </Form.Item>
                  <Divider />
                  <Form.Item>
                      <Title level={4}> Input your new password </Title>
                      <Input  prefix={<Icon type="lock"  style={{ color: 'rgba(0,0,0,.25)' }}/>} value={this.state.passwordNew} onChange={this.changePasswordNew}type="password" placeholder="Your new password"/>
                      <Input  prefix={<Icon type="lock"  style={{ color: 'rgba(0,0,0,.25)' }}/>} value={this.state.passwordNewCheck} onChange={this.changePasswordNewCheck  }type="password" placeholder="Your new password again"/>
                  </Form.Item>
                </Form>
              </Modal>
              </div>
          </div>
        </div>

    )
  }
}
export default UsersExamp;
