/*eslint-disable import/first*/
import React, {Component} from 'react';
import Editor from 'for-editor';
import { Typography, Input, Button,Alert, Icon } from 'antd';
const {Title} = Typography;
const {TextArea} = Input;
class TextEditor  extends Component {
  constructor(props){
    super(props)
    this.titleChange = this.titleChange.bind(this);
    this.descriptionChange = this.descriptionChange.bind(this);
    this.sendData = this.sendData.bind(this);
  };
 
  handleChange(value){  
    this.props.setArticleValue(value)
  };
  titleChange(event){
    this.props.setArticleTitle(event.target.value)
  }
  descriptionChange(event){
    this.props.setArticleDescription(event.target.value)
  }
  sendData(event){   
    this.props.createArticle(this.props.title, this.props.description, this.props.value)
  }
  render(){
    return (
      <div className="edit">
      <Title underline='true'> Create a new article...</Title>
      <div> 
        {
          this.props.status === 'ARTICLE RESOLVED' ? 
          <div> 
            <Alert type='success' message={
            <div> 
              <Title level={3}> 
                <Icon type="check-circle" style={{ fontSize: '50px'}} />
                Article created!
              </Title>
            </div>
          }/>
            </div> : 
          <div>{ this.props.status === 'ARTICLE REJECTED' ? 
            <div>
              <Alert type='error' message={
                <div>
                  <Icon type="warning"style={{ fontSize: '50px'}}/> 
                  <Title level={3}> Some thing went wrong!</Title>
                  </div>
              }/>
            
              
              </div> :
            <div></div> } 
          </div> 

        }
        </div>
        <div>
          <form>
            <Input 
              value={this.props.title}
              onChange={this.titleChange}
              className="inputs" 
              type="text" 
              placeholder="Name of a artile" 
              allowClear/>
            <TextArea 
              value={this.props.description} 
              onChange={this.descriptionChange}
              className="inputs" 
              placeholder="Few description words" 
              autosize/>
          </form>
        </div>
        <Editor 
          value={this.props.value}
          placeholder="Type here"    
          onChange={this.handleChange.bind(this)}/>
        <div className="btn-wrap">
          <Button 
            className="btn" 
            size='large'
            type="primary" 
            onClick={this.sendData} 
            block> Create 
          </Button>
        </div>
      </div>
    )
  }
}

export default TextEditor;
