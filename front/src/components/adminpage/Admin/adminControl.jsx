/*eslint-disable import/first*/
import React, {Component} from 'react';
import {Typography, Icon} from 'antd';
const {Title} = Typography;
import delay from '../../../delay'
class AdminControl extends Component{
  constructor (props){
    super(props);
    this.state = {
      articles : [],
      users : []
    }
  }
  async componentWillMount (){
    await delay(100);
    let data = { 
      articles : [],
      users : []
    }
   console.log( await ( await fetch('http://localhost:5000/readpage' )).json())
    this.setState({data});
    console.log(data)
  }
  render(){
    return(
      <div className="admin-eneter-controll">
        <div>
          <Title mark='true'> <Icon type="number" />ShareYN</Title>
          <Title level={3} underline='true' >Administarator mode </Title>
        </div>
          <div className="controll">
                <div>
                    <Title>Articles</Title>
                    {this.state.data  ? this.state.data.map(
                      article => 
                        <li> 
                          <div>
                            <span>{article.title}</span>
                            <span>{article.description}</span>
                          </div>
                        </li>,
                    console.log(this.state.data),):<Icon type="reload" style={{ fontSize: '100px'}} spin={true} />}
                </div>
                <div>
                    <Title>Users</Title>
                    {this.state.data ? this.state.data.map(
                      user => 
                      <li>
                        <div>
                          <span>{user.nickname}</span>
                          <span>{user.name}</span>
                          <span>{user.surname}</span>
                        </div>
                      </li>
                    ): <Icon type="reload" style={{ fontSize: '100px'}} spin={true} />}
                </div>
        </div>
          <div className='info'> 
            <span>You can write to me feedbacks or help making ShareYN better </span><br/>
            <span><a href='https://gitlab.com/Kalin1n?nav_source=navbar' target='__blank'> <Icon type="gitlab" /> ShareYN on gitlab</a> </span><br/>
            <span> <Icon type="wechat" /> kalinin_ivan_donetsk@ukr.net</span><br/>
            <span>  <Icon type="user" /> Kalinin Ivan, Kharkiv, 2019</span>
          </div>      
      </div>    
    )
    }
}

export default AdminControl;