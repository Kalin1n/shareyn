/*eslint-disable import/first*/
import React, {Component} from 'react';
import {Typography, Input, Form, Button, Divider, Icon } from 'antd';
const {Title} = Typography;
import {Link} from 'react-router-dom';
class Admin extends Component{
  constructor (props){
    super();
    this.nicknameChange = this.nicknameChange.bind(this);
    this.passwordChange = this.passwordChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  nicknameChange(event){
    this.props.setAdminLogin(event.target.value)
  }
  passwordChange (event){
    this.props.setAdminPassword(event.target.value)
  }
  handleSubmit (event){
    console.log("submited values : ", this.state)
  }
  render(){
    return(
      <div className="admin-eneter">
        <Title mark='true'> 
          <Icon type="number" />ShareYN
        </Title>
        <Title level={3} underline='true' >Enetering administarator mode </Title>
        <span> Please Input your admins login and password </span>
          <Form>
            <Form.Item>
              <Input 
                type="text" 
                placeholder='login' 
                value={this.props.login} 
                onChange={this.nicknameChange}
              />
            </Form.Item>
            <Form.Item>
              <Input 
                type='password' 
                placeholder='password' 
                value={this.props.password} 
                onChange={this.passwordChange}
              />
            </Form.Item>
            <Button > 
              <Link to='/admin/controll'>Enter as administrator</Link> 
            </Button>
            <Divider/>
            <Title level={3}> Or if you are not admin and get there by mistake go to our home page by this link
              <Link to='/main'> (　＾∇＾)</Link>
            </Title>
          </Form>
          <span>You can write to me feedbacks or help making ShareYN better </span>
          <Divider/>
          <span><a href='https://gitlab.com/Kalin1n?nav_source=navbar' target='__blank'> <Icon type="gitlab" /> ShareYN on gitlab</a> </span><br/>
          <span> <Icon type="wechat" /> kalinin_ivan_donetsk@ukr.net</span>
          <Divider />
          <span>  <Icon type="user" /> Kalinin Ivan, Kharkiv, 2019</span>
      </div>
    )
    }
}

export default Admin;
