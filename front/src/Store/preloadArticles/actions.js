export const GET_ARTICLES = 'GET_ARTICLES'

export const getArticles = () => ({
    type : GET_ARTICLES,
    payload : null
});