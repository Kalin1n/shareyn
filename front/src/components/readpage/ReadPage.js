/*eslint-disable import/first*/
import React,{ Component } from "react";
import PageBasic from '../Layout/layout'
import  delay  from '../../delay'
import { Typography, Icon } from 'antd';
const { Title } = Typography;
import { Link } from 'react-router-dom';
class ReadPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            data : []
        }
    }
    async componentWillMount(){
            await delay(100);
            let data = await ( await fetch('http://localhost:5000/readpage')).json()
            this.setState({ data })
            console.log(data);
    }
    render(){
        let root = this.props.match.url;
        return(
            <PageBasic>
                    <ul className='articles-ulka'> 
                        {this.state.data.length ? this.state.data.map(
                            article  => 
                            <li className='article-preload'>
                                <Link to={{
                                    pathname : `${root}/${article.title}`,
                                    state :     {component : 'Article'}
                                    }}>
                                    <Title level={2} className='title' underline={true}> {`${article.title}`}</Title>
                                    <Title level={3} className='description'> { `${article.description}`}</Title>
                                </Link>
                            </li>
                        ): <Icon type="reload" style={{ fontSize: '460px'}} spin={true} />}
                    </ul>
            </PageBasic>
        )
    }
}


// <Icon type="ellipsis" style={{ fontSize: '560px'}} spin={true}  />


export default ReadPage;
