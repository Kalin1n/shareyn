import React, {Component} from 'react';
import { Button, Divider } from 'antd';
import delay from '../../delay'


class AdminControll extends Component{
  constructor(props){
    super(props)
    this.state = {
      articles : [],
      users : []
    }
  }
  async componentDidMount (){
    await delay(100);
    let data = { 
      articles : [],
      users : []
    }
    data.articles = await ( await fetch('http://localhost:5000/readpage' )).json()
    this.setState({data});
    console.log(data)
  }
  render(){
    return (
      <div>
          <span> Admin controll page </span>
          <Button> Get all users </Button>
          <Divider/>
          <Button> Get all articles </Button>
          
      </div>
    )
  }
}

export default AdminControll;