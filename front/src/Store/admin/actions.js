export const ADMIN_LOGIN_CHANGE = 'ADMIN_LOGIN_CHANGE';
export const ADMIN_PASSWORD_CHANGE = 'ADMIN_PASSWORD ';
export const SET_STATUS = 'SET_STATUS';
export const setAdminLogin = (login) => ({
    type : ADMIN_LOGIN_CHANGE,
    payload : login
})

export const setAdminPassword = (password) => ({
    type : ADMIN_PASSWORD_CHANGE,
    payload : password
})

export const adminActionPending = () => ({
    type : SET_STATUS,
    status : 'PENDING',
    payload : null,
    error : null
}) 

export const adminActionResolved = ( payload ) =>({
    type : SET_STATUS,
    status : 'RESOLVED',
    payload,
    error : null
})

export const adminActionRejected = ( error ) => ({
    type : SET_STATUS,
    status : 'REJECTED',
    payload : null,
    error 
})
export function signAsAdmin ( login , password ){
    return async dispatch => {
        dispatch(adminActionPending())
        console.log('Admin sign In values : ', login, password);
        var data = await ( await fetch ('http://localhost:5000/signAsAdmin', {
            headers : {
                'Content-Type':"application/json",
                'Accept' :'application/json'
            },
            method : 'POST',
            body : JSON.stringify({ login, password})
        })).json()
        console.log('Admin sign in resolved data',data)
        return {
            type : SET_STATUS,
            payload :data
        }
    }
}