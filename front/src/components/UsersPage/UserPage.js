/*eslint-disable import/first*/
import React,{ Component } from "react";
import {Typography, Tabs} from 'antd';
const {Title} = Typography;
const TabPane = Tabs.TabPane;
import PageBasic from '../Layout/layout'
import UsersExamp from './User/User'

class UserPage extends Component{
    render(){
        return(
            <PageBasic>
                <Title underline='true'> Users info...</Title>
                <Tabs>
                <TabPane tab='Statisticks' key='2s'>
                  <h1>text</h1>
                </TabPane>
                <TabPane tab='Change users info' key='2'>
                  <UsersExamp/>
                </TabPane>
                </Tabs>
            </PageBasic>
        )
    }
}

export default UserPage;
