import React, {Component} from 'react';
import {connect} from 'react-redux'
import  Admin from './Admin/Admin'
import {setAdminLogin, setAdminPassword} from '../../Store/admin/actions'
class AdminPageContainer extends Component{
    render(){
        return (
            <Admin 
                login={this.props.login}
                password={this.props.password}
                setAdminLogin={this.props.setAdminLogin}
                setAdminPassword={this.props.setAdminPassword}
            />
        )
    }
}

const mapStateToProps = ( state ) => {
    return {
        login : state.admin.login,
        password : state.admin.password
    }
}
const mapDispatchToProps = {
    setAdminLogin, setAdminPassword
}
export default connect ( mapStateToProps, mapDispatchToProps )(AdminPageContainer)